<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getThumb')) {
    function getThumb($url){
        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            $url = base_url('media/'.$url);
            return $url;
        } else {
            return $url;
        }
    }
}