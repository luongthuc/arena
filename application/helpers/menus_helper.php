<?php

function menumain(){
    $ci = &get_instance();
    /// SETTING //
    $db_table = 'arena_menus';
    ///////////////
    $ci->db->select("*");
    $ci->db->where(["parent" => "0"]);
    $query = $ci->db->get($db_table);
    $data_menu = $query->result_array();
    $html = '';
    $htmlChild = '';
    for ($i = 0; $i < count($data_menu); $i++) {
        $htmlChild = '';
        if ($data_menu[$i]['child'] == 1) {
            $ci->db->select("*");
            $ci->db->where(["parent" => $data_menu[$i]['id']]);
            $queryChild = $ci->db->get($db_table);
            $data_menuChild = $queryChild->result_array();
            $htmlChild .= '<ul class="sub-menu">';
            for ($i2 = 0; $i2 < count($data_menuChild); $i2++) {
                $htmlChild .= '<li class=""><a href="' . base_url('/' . $data_menuChild[$i2]['url']) . '">' . $data_menuChild[$i2]['name'] . '</a></li>';
            }
            $htmlChild .= '</ul>';
        }
        
        if (strlen($htmlChild) > 0) {
            $html .= '<li class="has-children"><a href="' . base_url('/' . $data_menu[$i]['url']) . '">' . $data_menu[$i]['name'] . '</a>' . $htmlChild . '</li>';
        } else {
            $html .= '<li><a href="' . base_url('/' . $data_menu[$i]['url']) . '">' . $data_menu[$i]['name'] . '</a>' . $htmlChild . '</li>';
        }
        
    }
    $html .= '</ul></nav>';
    return $html;
}
