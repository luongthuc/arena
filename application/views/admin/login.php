<style>
.login-temp{
    background-color: rgba(255, 255, 255, .5); 
    position: fixed;
    z-index: 999;
    margin-left: -18%;
    left: 50%;
}
@media (max-width: 500px) {
    .login-temp{
    position: fixed;
    z-index: 999;
    left: 0;
    width: 100%;
    margin-left: 0;
    }
}
</style>
    <!-- Main Content -->
    <div id="app">
        <section class="section">
            <div class="d-flex flex-wrap align-items-stretch">
            <div class="col-lg-4 col-md-6 col-12 min-vh-100 login-temp">
                    <div class="p-4 m-3">
                        <h3>Dashboard</h3>
                        <form method="post" action="" class="needs-validation" novalidate="">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="email" class="form-control" name="email" tabindex="1" required="" autofocus="">
                                <div class="invalid-feedback">
                                    Email:
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                </div>
                                <input id="password" type="password" class="form-control" name="password" tabindex="2" required="">
                            </div>
                            <br>
                            <div class="form-group text-right">
                                <button type="submit" class="btn btn-block btn-success btn-lg btn-icon icon-right" tabindex="4">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
                <div class="col-lg-12 col-12 order-lg-2 order-1 min-vh-100 background-walk-y position-relative overlay-gradient-bottom"
                    data-background="<?php echo base_url('public/images/background/login-bg2.jpg'); ?>">
                    <div class="absolute-bottom-left index-2">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- End Main Content -->