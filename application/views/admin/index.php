<!-- Main Content -->
<div class="main-content">
    			<section class="section">
    				<div class="section-header">
    					<h1 style="font-size: 27px; letter-spacing:-0.5px; color:black;">Dashboard</h1>
    				</div>
    				<div class="row">
    					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
    						<div class="card card-statistic-1">
    							<div class="card-icon bg-primary">
    								<i class="far fa-user"></i>
    							</div>
    							<div class="card-wrap">
    								<div class="card-header">
    									<h4>QUẢN TRỊ VIÊN</h4>
    								</div>
    								<div class="card-body">
    									<?php echo $this->db->count_all('siswa'); ?>
    								</div>
    							</div>
    						</div>
    					</div>
    					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
    						<div class="card card-statistic-1">
    							<div class="card-icon bg-danger">
    								<i class="fas fa-chalkboard-teacher"></i>
    							</div>
    							<div class="card-wrap">
    								<div class="card-header">
    									<h4>NGƯỜI DÙNG</h4>
    								</div>
    								<div class="card-body">
    									<?php echo $this->db->count_all('guru'); ?>
    								</div>
    							</div>
    						</div>
    					</div>
    					<div class="col-lg-4 col-md-6 col-sm-6 col-12">
    						<div class="card card-statistic-1">
    							<div class="card-icon bg-warning">
    								<i class="fas fa-book"></i>
    							</div>
    							<div class="card-wrap">
    								<div class="card-header">
    									<h4>BÀI VIẾT</h4>
    								</div>
    								<div class="card-body">
    									<?php echo $this->db->count_all('posts'); ?>
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
    			</section>
    		</div>