<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="">
            <div class="card" style="width:100%;">
                <div class="card-body">
                    Thêm bài viết
                </div>
            </div>
            <div class="card card-success">
                <div id="detail" class="card-body">
                    <div class="col-md-12 bg-white" style="border-radius:3px;box-shadow:rgba(0, 0, 0, 0.03) 0px 4px 8px 0px">
                        <form method="post" enctype="multipart/form-data" action="<?php echo base_url('admin/posts/add'); ?>">
                            <input type="hidden" name="id">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputEmail4">Tiêu đề</label>
                                            <input autocomplete="off" required type="text" list="nama_guru" id="namaguru" name="nama_guru" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Mô tả</label>
                                        <input type="text" class="form-control" name="nama_mapel" id="nama_mapel" required placeholder="Pilih nama guru yang valid agar nama mapel muncul">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Nội dung</label>
                                        <textarea class="form-control" required name="deskripsi" id="exampleFormControlTextarea1" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                      <label for="typeCategory">Danh mục</label>
                                      <select class="form-control h-auto" name="category" id="typeCategory">
                                      <?php if(!empty($category)) foreach ($category as $k => $val): ?>
                                        <option value="<?php echo $k; ?>"><?php echo $val['name']; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 offset-6">
                                    <button type="submit" class="btn btn-block btn-success">Tạo mới</button></div>
                            </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </section>
</div>
</div>
<!-- End Main Content -->