<!-- Main Content -->
<div class="main-content">
	<section class="section">
		<div class="card" style="width:100%;">
			<div class="card-body">
				<?php if(!empty($data)) echo $data['title']; ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="bg-white p-4" style="border-radius:3px;box-shadow:rgba(0, 0, 0, 0.03) 0px 4px 8px 0px">
					<div class="table-responsive">
						<table id="example" class="table align-items-center table-flush">
							<thead class="thead-light">
								<tr class="text-center">
									<?php if(!empty($data)) foreach ($data['tb_head'] as $val): ?>
                                        <th scope="col">
                                            <?php echo $val; ?>
                                        </th>
                                    <?php endforeach; ?>
									<th scope="col" colspan="2">Option</th>
								</tr>
							</thead>
							<tbody>
                                    <?php if(!empty($showDB)) foreach ($showDB as $val):  ?>
                                        <tr>
                                        <?php if(!empty($data)) foreach ($data['tb_body'] as $tb): ?>
                                        <td scope="col">
											<?php if($tb == 'category'): ?>
												<?php echo $category[$val['category']]['name']; ?>
											<?php else: ?>
											<?php echo substr($val[$tb], 0, 50); ?>
											<?php endif; ?>
                                        </td>
                                        <?php endforeach; ?>
                                        <td class="text-center">
										<a href="<?php echo site_url('admin/posts/update/' . $val['id']); ?>"
										class="btn btn-info">Sửa</a>
									    </td>
                                        <td>
                                        <a href="<?php echo site_url('admin/posts/delete/' . $val['id']); ?>"
											class="btn btn-danger remove">Xóa</a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
</div>
<!-- End Main Content -->
