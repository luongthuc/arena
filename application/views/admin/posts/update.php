            <!-- Main Content -->
            <div class="main-content">
            	<section class="section">
            		<div class="card card-success">
            			<?php foreach ($user as $u) { ?>
            				<div class="card-body">
            					<form method="POST" action="<?= base_url('admin/posts/save') ?>">
            						<input type="hidden" name="id" value="<?= $u['id'] ?>">
            						<div class="row">
            							<div class="col-6">
            								<div class="form-group">
            									<label for="nip">Tiêu đề</label>
            									<input id="nama_guru" type="text" class="form-control" value="<?= $u['title'] ?>" name="nama_guru">
            									<?= form_error('nama_guru', '<small class="text-danger">', '</small>'); ?>
            									<div class="invalid-feedback">
            									</div>
            								</div>
            								<div class="form-group">
            									<label for="nama_mapel">Mô tả</label>
            									<input id="nama_mapel" type="text" value="<?= $u['description'] ?>" class="form-control" name="nama_mapel">
            									<?= form_error('email', '<small class="text-danger">', '</small>'); ?>
            									<div class="invalid-feedback">
            									</div>
            								</div>
            								<div class="form-group">
            									<label for="exampleFormControlTextarea1">Nội dung</label>
            									<textarea class="form-control txtarea" name="deskripsi" id="exampleFormControlTextarea1" rows="3">
                                        <?= $u['content'] ?></textarea>
            								</div>
            							</div>
            							<div class="col-6">
            								<div class="form-group">
            									<label for="typeCategory">Danh mục</label>
            									<select class="form-control h-auto" name="category" id="typeCategory">
            										<?php if (!empty($category)) foreach ($category as $k => $val) : ?>
            											<option value="<?php echo $k; ?>"><?php echo $val['name']; ?></option>
            										<?php endforeach; ?>
            									</select>
            								</div>
            							</div>
            						</div>
            						<div class="row">
            							<div class="col-6 offset-6">
            								<div class="form-group">
            									<button type="submit" class="btn btn-success btn-lg btn-block">
            										Update ⭢
            									</button>
            								</div>
            							</div>
            						</div>
            					</form>
            				<?php } ?>
            				</div>
            		</div>
            	</section>
            </div>
            <!-- End Main Content -->