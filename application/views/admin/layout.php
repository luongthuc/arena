<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Dashboard</title>
    <link rel="icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/png">
    <!-- General CSS Files -->
    <link rel="stylesheet" href="<?php echo base_url('public/admin/css/bootstrap.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('public/admin/css/fontawesome/css/all.min.css')?>">
    <!-- Template CSS -->
    <link rel="stylesheet" href="<?php echo base_url('public/admin/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/admin/css/components.css'); ?>">
</head>

<body>

        <!-- Start Sidebar -->
        <div id="app">
    	<div class="main-wrapper">
    		<div class="navbar-bg"></div>
    		<!-- End Sidebar -->
            <?php $this->load->view('admin/_header'); ?>
            <?php echo $main_content; ?>
            <?php $this->load->view('admin/_footer'); ?>
    	</div>
    </div>

    <script src="<?php echo base_url('public/admin/js/sweetalert2.all.min.js'); ?>"></script>
    <!-- General JS Scripts -->
    <script src="<?php echo base_url('public/admin/js/jquery-3.3.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/admin/js/popper.js'); ?>"></script>
    <script src="<?php echo base_url('public/admin/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/admin/js/nicescroll.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/admin/js/moment.min.js'); ?>"></script>
    <script src="<?php base_url('public/admin/js/stisla.js'); ?>"></script>
    <!-- Template JS File -->
    <script src="<?=base_url('assets/')?>stisla-assets/js/scripts.js"></script>
    <script src="<?php base_url('public/admin/js/custom.js'); ?>"></script>
    <!-- Page Specific JS File -->
</body>

</html>