<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="">
            <div class="card" style="width:100%;">
                <div class="card-body">
                    Thêm danh mục
                </div>
            </div>
            <div class="card card-success">
                <div id="detail" class="card-body">
                    <div class="col-md-12 bg-white" style="border-radius:3px;box-shadow:rgba(0, 0, 0, 0.03) 0px 4px 8px 0px">
                        <form method="post" enctype="multipart/form-data" action="">
                            <input type="hidden" name="id">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label for="inputEmail4">Tên danh mục</label>
                                            <input autocomplete="off" required type="text" list="nama_guru" id="namaguru" name="name" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                      <label for="typeCategory">Loại Danh mục</label>
                                      <select class="form-control h-auto" name="type" id="typeCategory">
                                      <?php if(!empty($data)) foreach ($data['type'] as $k => $val): ?>
                                        <option value="<?php echo $k; ?>"><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 offset-6">
                                    <button type="submit" class="btn btn-block btn-success">Tạo mới</button></div>
                            </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
    </section>
</div>
</div>
<!-- End Main Content -->