            <!-- Main Content -->
            <div class="main-content">
            	<section class="section">
            		<div class="card card-success">
            			<div class="card-body">
            				<form method="POST" action="<?= base_url('admin/category/posts/save') ?>">
								<input type="hidden" name="id" value="<?php if(!empty($id)) echo $id; ?>">
								<?php if(!empty($data)) foreach ($data['tb_body'] as $k => $val): if($val != 'id'): ?>
									<?php if($val == 'type'): ?>
									<div class="form-group">
                                      <label for="typeCategory">Loại Danh mục</label>
                                      <select class="form-control h-auto" name="type" id="typeCategory">
                                      <?php if(!empty($data)) foreach ($data['type'] as $k => $val): ?>
                                        <option value="<?php echo $k; ?>"><?php echo $val; ?></option>
                                        <?php endforeach; ?>
                                      </select>
									</div>
									  <?php else: ?>
									<div class="form-group">
            							<label for="nip"><?php if(!empty($val)) echo $data['tb_head'][$k]; ?></label>
            							<input id="nama_guru" type="text" class="form-control"
            								value="<?php if(!empty($cat)) echo $cat[0][$val]; ?>" name="<?php if(!empty($val)) echo $val; ?>">
            							<?= form_error('nama_guru', '<small class="text-danger">', '</small>'); ?>
            							<div class="invalid-feedback"></div>
            						</div>
									  <?php endif;endif; endforeach; ?>
            					
            					<div class="form-group">
            						<button type="submit" class="btn btn-success btn-lg btn-block">
            							Update ⭢
            						</button>
            					</div>
            				</form>
            			</div>
            		</div>
            	</section>
            </div>
            <!-- End Main Content -->