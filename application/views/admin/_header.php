<?php if(!empty($this->session->userdata('email'))): ?>
    <nav class="navbar navbar-expand-lg main-navbar">
	<form class="form-inline mr-auto">
		<ul class=" navbar-nav mr-3">
			<li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a>
			</li>
		</ul>
	</form>
	<ul class="navbar-nav navbar-right">
		<li class="dropdown"><a href="#" data-toggle="dropdown"
				class="nav-link dropdown-toggle nav-link-lg nav-link-user">
				<img alt="image" style="margin-bottom:4px !important;"
					src="<?= base_url('assets/stisla-assets/img/avatar/avatar-2.png') ?>"
					class="rounded-circle mr-1 my-auto border-white">
				<div class="d-sm-none d-lg-inline-block" style="font-size:15px;">
                <?php
                $data['user'] = $this->db->get_where('admin', ['email' => $this->session->userdata('email')])->row_array(); echo $data['user']['username']; ?>
                </div>
			</a>
			<div class="dropdown-menu dropdown-menu-right">
				<div class="dropdown-title">Admin - Learnify</div>
				<a href="<?= base_url('admin/dashboard/logout') ?>" class="dropdown-item has-icon text-danger">
					<i class="fas fa-sign-out-alt"></i> Logout
				</a>
			</div>
		</li>
	</ul>
</nav>
<div class="main-sidebar">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand text-danger">
			<div>
				<a href="<?= base_url('admin') ?>"
					style="font-size: 30px;font-weight:900;font-family: 'Poppins', sans-serif;"
					class="text-success text-center"><i style="font-size: 30px;" class="fas fa-graduation-cap"></i> Dashboard</a>
			</div>
		</div>
		<div class="sidebar-brand sidebar-brand-sm">
			<a href="<?= base_url('admin') ?>">AD</a>
		</div>
		<ul class="sidebar-menu">
			<li class="menu-header ">Dashboard</li>
			<li class="nav-item dropdown active">
				<a href="<?= base_url('admin') ?>" class="nav-link"><i
						class="fas fa-desktop"></i><span>Dashboard</span></a>
			</li>
			<li class="menu-header">Quản lý bài viết</li>
			<li class="nav-item dropdown">
				<a href="#" class="nav-link has-dropdown"><i class="fas fa-book"></i>
					<span>Danh sách</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?= base_url('admin/posts/data') ?>">Bài viết</a>
					</li>
					<li><a class="nav-link" href="<?= base_url('admin/posts/add') ?>">Tạo mới</a>
					</li>
				</ul>
			</li>
			<li class="nav-item dropdown">
				<a href="#" class="nav-link has-dropdown"><i class="fas fa-book"></i>
					<span>Danh mục</span></a>
				<ul class="dropdown-menu">
					<li><a class="nav-link" href="<?= base_url('admin/category/posts') ?>">Bài viết</a>
					</li>
					<li><a class="nav-link" href="<?= base_url('admin/category/posts/add') ?>">Tạo mới</a>
					</li>
				</ul>
			</li>
	</aside>
</div>
<?php endif; ?>