<?php if(!empty($this->session->userdata('email'))): ?>
<!-- Start Footer -->
<footer class="main-footer">
	<div class="text-center">
		Copyright &copy; 2020 <div class="bullet"></div> AcmaTvirus
	</div>
</footer>
<!-- End Footer -->
<?php endif; ?>