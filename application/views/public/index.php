<div class='row' id='content-wrapper'>
    <div class='clear'></div>
    <div id='main-wrapper'>
        <div class='slider-wrapper' id='slider-wrapper'>
            <div class='slider-sec section' id='slider-sec' name='Main Slider'>
                <div class='widget HTML' data-version='1' id='HTML3'>
                    <div class="widget-title">
                        <h2 class='title'>Latest in Tech</h2>
                    </div>
                    <div class='widget-content'>
                        <div class="recent" data-results="4"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class='main section' id='main' name='Main Posts'>
            <div class='widget Blog' data-version='1' id='Blog1'>
                <div class='blog-posts hfeed'>

                    <?php if (!empty($newspost)) foreach ($newspost as $key => $item) : ?>
                        <div class='post-outer'>
                            <div class='post'>
                                <div class='block-image'>
                                    <div class='thumb'>
                                        <a href='' style='background:url(<?php echo getThumb($item['thumb']); ?>) no-repeat center center;background-size:cover'></a>
                                    </div>
                                </div>
                                <div class='post-header'>
                                </div>
                                <article>
                                    <font class='retitle'>
                                        <h2 class='post-title entry-title'>
                                            <a href=''>
                                                <?php echo $item['title']; ?>
                                            </a>
                                        </h2>
                                    </font>
                                    <div class='date-header'>
                                        <div id='meta-post'>
                                            <a class='timestamp-link' href='' rel='bookmark' title='permanent link'><abbr class='published' itemprop='datePublished dateModified' title='August 01, 2016'>
                                                </abbr></a>
                                            <div class='resumo'><span>
                                                    <?php echo $item['description']; ?>
                                                </span></div>
                                        </div>
                                        <div style='clear: both;'></div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="blog-pager" id="blog-pager"><span class="showpageOf">Page 1 of 3</span><span class="showpagePoint">1</span><span class="showpageNum"><a href="#" onclick="redirectpage(2);return false">2</a></span><span class="showpageNum"><a href="#" onclick="redirectpage(3);return false">3</a></span><span class="showpageNum"><a href="#" onclick="redirectpage(2);return false">NEXT</a></span></div>
            </div>
            <div class='widget HTML' data-version='1' id='HTML300'>
            </div>
            <div class='widget HTML' data-version='1' id='HTML301'>
            </div>
        </div>
    </div>
    <?php echo $sidebar; ?>
</div>
<!-- end content-wrapper -->
<div class='clear'></div><!-- Footer wrapper -->