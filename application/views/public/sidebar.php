<div id='sidebar-wrapper'>
    <div class='sidebar ready-widget section' id='sidebar2' name='Sidebar Right'>
        <div class='widget PopularPosts HTML' data-version='1' id='HTML1'>
            <div class="widget-title">
                <h2>Random Posts</h2>
            </div>

            <div class='widget-content popular-posts'>
                <ul>
                    <?php if (!empty($RandomPosts)) foreach ($RandomPosts as $key => $item) : ?>
                        <li>
                            <div class='item-content'>
                                <div class='item-thumbnail'>
                                    <a href=''>
                                        <img alt='<?php echo $item['title']; ?>' border='0' height='72' src='<?php echo getThumb($item['thumb']); ?>' width='72' />
                                    </a>
                                </div>
                                <div class='item-title'><a href=''><?php echo $item['title']; ?></a></div>
                                <div class='item-snippet'><?php echo $item['description']; ?></div>
                            </div>
                            <div style='clear: both;'></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class='widget HTML' data-version='1' id='HTML2'>
            <div class="widget-title">
                <h2 class='title'>Follow Us</h2>
            </div>

            <div class='widget-content'>
                <div class="social-count-plus">
                    <ul class="default">
                        <li class="count-facebook">
                            <a class="icon" href="https://www.facebook.com/WordPress"></a>
                            <span class="items" style="color: #fff"><span class="count" style="color: #fff">1113957</span>
                                <span class="label" style="color: #fff">likes</span></span></li>
                        <li class="count-twitter"><a class="icon" href="https://twitter.com/AlxMedia"></a><span class="items" style="color: #fff"><span class="count" style="color: #fff">441</span><span class="label" style="color: #fff">followers</span></span></li>
                        <li class="count-youtube"><a class="icon" href="https://www.youtube.com/user/Envato"></a><span class="items" style="color: #fff"><span class="count" style="color: #fff">758</span><span class="label" style="color: #fff">subscribers</span></span></li>
                    </ul>
                </div>
            </div>
            <div class='clear'></div>
            <span class='widget-item-control'>
                <span class='item-control blog-admin'>
                    <a class='quickedit' href='http://www.blogger.com/rearrange?blogID=4819543551673866339&widgetType=HTML&widgetId=HTML2&action=editWidget&sectionId=sidebar2' onclick='return _WidgetManager._PopupConfig(document.getElementById("HTML2"));' rel='nofollow' target='configHTML2' title='Edit'>
                        <img alt='' height='18' src='https://resources.blogblog.com/img/icon18_wrench_allbkg.png' width='18' />
                    </a>
                </span>
            </span>
            <div class='clear'></div>
        </div>
        <div class='widget HTML' data-version='1' id='HTML7'>
            <div class="widget-title">
                <h2 class='title'>Recent Comments</h2>
            </div>

            <div class='widget-content'>
                <div class="recentcomments"></div>
                <span id="blog-pager-older-link">
                    <a class="blog-pager-older-link" href="" id="Blog1_blog-pager-older-link" title="Older Posts">Older Comment</a>
                </span>
            </div>
        </div>
        <div class='widget FollowByEmail' data-version='1' id='FollowByEmail1'>
            <h2 class='title'>Register new account</h2>
            <div class='widget-content'>
                <div class='follow-by-email-inner'>
                    <form action='https://feedburner.google.com/fb/a/mailverify' method='post' onsubmit='window.open("https://feedburner.google.com/fb/a/mailverify?uri=blogspot/qftaF", "popupwindow", "scrollbars=yes,width=550,height=520"); return true' target='popupwindow'>
                        <table width='100%'>
                            <tr>
                                <td>
                                    Username <br>
                                    <input class='follow-by-email-address' name='email' placeholder='Email address...' type='text' />
                                </td>
                                <td>
                                    Password <br>
                                    <input class='follow-by-email-address' name='email' placeholder='Email address...' type='text' />
                                </td>
                                <td width='64px'>
                                    <input class='follow-by-email-submit' type='submit' value='Submit' />
                                </td>
                            </tr>
                        </table>
                        <input name='uri' type='hidden' value='blogspot/qftaF' />
                        <input name='loc' type='hidden' value='en_US' />
                    </form>
                </div>
            </div>
            <span class='item-control blog-admin'>
            </span>
        </div>
        <div class='widget PopularPosts' data-version='1' id='PopularPosts1'>

            <div class="widget-title">
                <h2>Popular Posts</h2>
            </div>
            <div class='widget-content popular-posts'>
                <ul>
                    <?php if (!empty($PopularPosts)) foreach ($PopularPosts as $key => $item) : ?>
                        <li>
                            <div class='item-content'>
                                <div class='item-thumbnail'>
                                    <a href=''>
                                        <img alt='<?php echo $item['title']; ?>' border='0' height='72' src='<?php echo getThumb($item['thumb']); ?>' width='72' />
                                    </a>
                                </div>
                                <div class='item-title'><a href=''><?php echo $item['title']; ?></a></div>
                                <div class='item-snippet'><?php echo $item['description']; ?></div>
                            </div>
                            <div style='clear: both;'></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class='widget Label' data-version='1' id='Label2'>
            <div class="widget-title">
                <h2>Tags</h2>
            </div>

            <div class='widget-content cloud-label-widget-content'>
                <?php if (!empty($tag)) foreach ($tag as $key => $item) : ?>
                    <span class='label-size label-size-5'>
                        <a dir='ltr' href=''>
                            <?php echo $item['tag']; ?>
                        </a>
                    </span>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<div class='clear'></div>