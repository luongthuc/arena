<div id='main-header'>
    <div class='tm-head group row'>
        <div id='logo-wrap'>
            <div class='section' id='header' name='Logo'>
                <div class='widget Header' data-version='1' id='Header1'>
                    <div id='header-inner'>
                        <a href=''>
                            <h1 class="Frightmare">ARENA</h1>
                        </a>
                    </div>
                </div>
            </div>
            <span id='tagline'></span>
        </div>
        <div id='header-elements'>
            <div id='header-social'>
                <div class='social-top section' id='social-top' name='Social Top'>
                    <div class='widget LinkList' data-version='1' id='LinkList47'>
                        <div class='widget-content'>
                            <ul>
                                <li><a href='http://facebook.com' title='facebook'><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href='http://twitter.com' title='twitter'><i class="fab fa-twitter"></i></a></li>
                                <li><a href='http://Instagram.com' title='instagram'><i class="fab fa-instagram"></i></a></li>
                                <li><a href='http://feed.rss.com' title='rss'><i class="fas fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id='header-search'>
                <span id='search-icon'><i class='fa fa-search'></i></span>
                <div id='nav-search'>
                    <form action='/search/max-results=7' id='searchform' method='get'>
                        <input id='s' name='q' placeholder='Enter search term...' type='text' />
                        <input id='searchsubmit' type='submit' value='Search' />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Menu -->
<nav id='main-nav'>
    <div class='header-menu row'>
        <li class='li-home'><a href='<?php echo base_url(); ?>'>Home</a></li>
        <div class='section' id='menu' name='Main Menu'>
            <div class='widget LinkList' data-version='1' id='LinkList96'>
                <div class='widget-content'>
                    <ul id="menu-main-nav">
                        <?php echo menumain(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- Main Menu (end) -->