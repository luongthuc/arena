<!DOCTYPE html>
<html>

<head>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport' />
    <meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
    <link href='public/icon/favicon.ico' rel='icon' type='image/x-icon' />
    <title></title>
    <link rel="stylesheet" href="<?php echo base_url('public/css/toastr.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/css/style.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('public/css/arena.css'); ?>">
    <link href='<?php echo base_url('public/vendor/font-awesome/css/all.min.css'); ?>' rel='stylesheet' />
    <style type="text/css">
        /* Chart.js */
        @-webkit-keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }

            to {
                opacity: 1
            }
        }

        @keyframes chartjs-render-animation {
            from {
                opacity: 0.99
            }

            to {
                opacity: 1
            }
        }

        .chartjs-render-monitor {
            -webkit-animation: chartjs-render-animation 0.001s;
            animation: chartjs-render-animation 0.001s;
        }
    </style>
</head>

<body>
    <?php $this->load->view('public/_header'); ?>
    <?php if (!empty($main)) echo $main; ?>
    <?php $this->load->view('public/_footer'); ?>
    <div id="back-to-top"></div>
    <script src='<?php echo base_url('public/vendor/jquery-3.5.1.min.js'); ?>' type='text/javascript'></script>
    <script src="<?php echo base_url('public/js/script.js'); ?>"></script>
    <script src="<?php echo base_url('public/js/charts/chart.min.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/js/charts/chartjs/bar/column.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/js/charts/chartjs/bar/bar.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/js/charts/chartjs/line/line.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/js/charts/chartjs/pie-doughnut/pie-simple.js'); ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('public/js/charts/chartjs/pie-doughnut/doughnut-simple.js'); ?>" type="text/javascript"></script>
</body>

</html>