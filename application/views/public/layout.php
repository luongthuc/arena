<!DOCTYPE html>
<html>

<head>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport' />
    <meta content='text/html; charset=UTF-8' http-equiv='Content-Type' />
    <link href='public/icon/favicon.ico' rel='icon' type='image/x-icon' />
    <title>Arena baze</title>
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/arena.css">
    <link href='public/vendor/font-awesome/css/all.min.css' rel='stylesheet' />
</head>

<body class='index'>
    <?php $this->load->view('public/_header'); ?>
    <?php if (!empty($main)) echo $main; ?>
    <?php $this->load->view('public/_footer'); ?>
    <div id="back-to-top"></div>
    <script src='public/vendor/jquery-3.5.1.min.js' type='text/javascript'></script>
    <script src="public/js/script.js"></script>
</body>

</html>