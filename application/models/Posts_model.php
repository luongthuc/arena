<?php

class Posts_model extends CI_Model
{
    //////////////////////////////////////////
    public function loadNewPosts()
    {
        $this->db->from('posts');
        $this->db->order_by("id", "DESC");
        $this->db->limit(10);
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

    public function loadPopularPosts()
    {
        $this->db->from('posts');
        $this->db->order_by("id", "DESC");
        $this->db->limit(3);
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }

    public function loadRandomPosts()
    {
        $this->db->from('posts');
        $this->db->order_by('id', 'RANDOM');
        $this->db->limit(3);
        $query = $this->db->get();
        $data = $query->result_array();
        return $data;
    }
    //////////////////////////////////////////

    public function getAllData()
    {
        $this->db->limit(10);
        return $this->db->get('posts')->result_array();
    }

    public function deleteRow($id)
    {
        $this->db->where(['id' => $id]);
        $this->db->delete('posts');
    }

    public function getData($where)
    {
        return $this->db->get_where('posts', $where)->result_array();
    }

    public function updateData($where, $data)
    {
        $this->db->where($where);
        $this->db->update('posts', $data);
    }
}
