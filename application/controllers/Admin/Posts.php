<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posts extends CI_Controller
{
    
    protected $_data;
    protected $_category;

    public function __construct()
    {
        parent::__construct();
        // Load library
        $md = $this->load->model(['posts_model', 'category_model']);
        $this->_data = new Posts_model();
        $this->_category = new Category_model();
    }

    private function config() {
        $data['data'] = [
            'title' => 'Danh sách bài viết',
            'tb_head' => ['id', 'Tiêu đề', 'Mô tả', 'Nội dung', 'Danh mục'],
            'tb_body' => ['id', 'title', 'description', 'content', 'category']
        ];
        return $data;
    }

    public function data()
    {
        $data = $this->config();
        $data['category'] = $this->_category->getAllData();
        $data['showDB'] = $this->_data->getAllData();
        $data['main_content'] = $this->load->view('admin/posts/data', $data, true);
        $this->load->view('admin/layout', $data);
    }

    public function update($id)
    {
        $data = $this->config();
        $data['category'] = $this->_category->getAllData();
        $data['user'] = $this->_data->getData(['id'=>$id]);
        $data['main_content'] = $this->load->view('admin/posts/update', $data, true);
        $this->load->view('admin/layout', $data);
    }

    public function delete($id)
    {
        $this->_data->deleteRow($id);
        $this->session->set_flashdata('user-delete', 'berhasil');
        redirect('admin/posts/data');
    }

    public function add()
    {
        $data = $this->config();
        $data['category'] = $this->_category->getAllData();
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim|min_length[1]', [
            'required' => 'Harap isi kolom deskripsi.',
            'min_length' => 'deskripsi terlalu pendek.',
        ]);
        if ($this->form_validation->run() == false) {
            $data['main_content'] = $this->load->view('admin/posts/add', $data, true);
            $this->load->view('admin/layout', $data);
        } else {
            $data = [
                'title' => htmlspecialchars($this->input->post('nama_guru', true)),
                'description' => htmlspecialchars($this->input->post('nama_mapel', true)),
                'video' => '',
                'content' => htmlspecialchars($this->input->post('deskripsi', true)),
                'category' => htmlspecialchars($this->input->post('category', true)),
            ];

            $this->db->insert('posts', $data);
            $this->session->set_flashdata('success-reg', 'Tạo thành công!');
            redirect('admin/posts/data');
        }
    }

    public function save()
    {
        $id = $this->input->post('id');
        $nama_guru = $this->input->post('nama_guru');
        $nama_mapel = $this->input->post('nama_mapel');
        $deskripsi = $this->input->post('deskripsi');

        $data = array(
            'title' => $nama_guru,
            'description' => $nama_mapel,
            'content' => $deskripsi,
            'category' => $this->input->post('category')
        );

        $where = array(
            'id' => $id,
        );

        $this->_data->updateData($where, $data);
        $this->session->set_flashdata('success-edit', 'berhasil');
        redirect('admin/posts/data');
    }
}
