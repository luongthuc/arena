<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{

    protected $_data;

    public function __construct()
    {
        parent::__construct();
        // Load library
        $md = $this->load->model(['Category_model']);
        $this->_data = new Category_model();
    }

    public function posts($key = null, $id = null)
    {
        $data['data'] = [
            'title' => 'Danh mục bài viết',
            'tb_head' => ['id', 'name', 'type'],
            'tb_body' => ['id', 'name', 'type'],
            'type' => [
                'posts' => 'Bài viết chung'
            ]
        ];
        switch ($key) {
            case 'add':
                $this->form_validation->set_rules('name', 'Deskripsi', 'required|trim|min_length[1]', [
                    'required' => 'Harap isi kolom deskripsi.',
                    'min_length' => 'deskripsi terlalu pendek.',
                ]);
                if ($this->form_validation->run() == false) {
                    $data['main_content'] = $this->load->view('admin/category/add', $data, true);
                    $this->load->view('admin/layout', $data);
                } else {
                    $data = [
                        'name' => htmlspecialchars($this->input->post('name', true)),
                        'type' => htmlspecialchars($this->input->post('type', true)),
                    ];
                    $this->db->insert('posts_category', $data);
                    redirect(base_url('admin/category/posts'));
                }
                break;

            case 'update':
                $data['id'] = $id;
                $data['cat'] = $this->_data->getData(['id' => $id]);
                $data['main_content'] = $this->load->view('admin/category/update', $data, true);
                break;
            case 'save':
                $data = [
                    'name' => htmlspecialchars($this->input->post('name', true)),
                    'type' => htmlspecialchars($this->input->post('type', true)),
                ];
                $where = array(
                    'id' => $this->input->post('id'),
                );

                $this->_data->updateData($where, $data);
                $this->session->set_flashdata('success-edit', 'berhasil');
                redirect(base_url('admin/category/posts'));
                break;
            case 'delete':
                $this->_data->deleteRow($id);
                $this->session->set_flashdata('user-delete', 'berhasil');
                redirect(base_url('admin/category/posts'));
                break;
            default:
                $data['category'] = $this->_data->getAllData();
                $data['main_content'] = $this->load->view('admin/category/posts', $data, true);
                break;
        }
        $this->load->view('admin/layout', $data);
    }
}
