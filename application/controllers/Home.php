<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    protected $_post;
    protected $_category;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Posts_model', 'Category_model']);
        $this->_post = new Posts_model();
        $this->_category = new Category_model();
    }

    public function index()
    {
        $data = [];
        $data['newspost'] = $this->_post->loadNewPosts();
        $data['PopularPosts'] = $this->_post->loadPopularPosts();
        $data['RandomPosts'] = $this->_post->loadRandomPosts();
        $data['tag'] = $this->_category->loadTag();
        $data['sidebar'] = $this->load->view('public/sidebar', $data, true);
        $data['main'] = $this->load->view('public/index', $data, true);
        $this->load->view('public/layout', $data);
    }
}
