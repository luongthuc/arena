<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends MY_Controller
{
    protected $_post;
    protected $_category;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Posts_model', 'Category_model']);
        $this->_post = new Posts_model();
        $this->_category = new Category_model();
    }

    public function index($page)
    {
        $data = [];
        
        $data['main'] = $this->load->view('public/page/'.$page, $data, true);
        $this->load->view('public/layout', $data);
    }
}
