<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lottery extends MY_Controller
{
    protected $_post;
    protected $_category;
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Lottery_model', 'Category_model']);
        $this->_post = new Lottery_model();
        $this->_category = new Category_model();
    }

    public function statistic()
    {
        $data = [];
        $data['data'] = $this->match();
        dump($data);
        $data['main'] = $this->load->view('public/lottery/index', $data, true);
        $this->load->view('public/lottery/layout', $data);
    }

    private function match() {
        $code = '';
        $dataResult = $this->_post->getResultDtoD('01/01/2018', '01/01/2019');
        if (!empty($dataResult)) foreach ($dataResult as $key => $item) {
            $item = (object) $item;
            $result = json_decode($item->data_result, true);
            if (!empty($result[1][0])) {
                if ($code === 'xsmb') $rewardDB = $result[1][0];
                else $rewardDB = $result[8][0];
                $data2Number[] = substr($rewardDB, -2, 2);
            }
        }
        $new2so = [];
        $new_text = '';
        foreach ($data2Number as $k => $value) {
            $so2 = substr($value, 0, 2);

            $new_text .= '-' . $so2 . '-';
            if (substr_count($new_text, $so2) == 1) {
                array_push($new2so, substr($value, 0, 2));
            }
        }
        $newk2 = [];
        foreach ($new2so as $value) {
            $count = substr_count($new_text, $value);
            array_push($newk2, ['number' => $value, 'count' => $count]);
        }
        usort($newk2, function ($a, $b) {
            return $a['count'] < $b['count'] ? 1 : -1;
        });
        return $newk2;
    }
}
