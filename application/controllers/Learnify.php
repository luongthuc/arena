<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Learnify extends CI_Controller
{

    public function index()
    {
      $this->load->view('Learnify/nav');
      $this->load->view('Learnify/index');
      $this->load->view('Learnify/footer');
    }
}
