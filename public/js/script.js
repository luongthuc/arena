function setAttributeOnload(object, attribute, val) {
    if (window.addEventListener) {
        window.addEventListener('load',
            function() {
                object[attribute] = val;
            }, false);
    } else {
        window.attachEvent('onload', function() {
            object[attribute] = val;
        });
    }
}

(function(c) {
    c.fn.hoverTimeout = function(d, e, f, g) {
        return this.each(function() {
            var a = null,
                b = c(this);
            b.hover(function() {
                clearTimeout(a);
                a = setTimeout(function() {
                    e.call(b)
                }, d)
            }, function() {
                clearTimeout(a);
                a = setTimeout(function() {
                    g.call(b)
                }, f)
            })
        })
    }
})(jQuery);

(function(e) {
    e.fn.replaceText = function(t, n, r) {
        return this.each(function() {
            var i = this.firstChild,
                s, o, u = [];
            if (i) {
                do {
                    if (i.nodeType === 3) {
                        s = i.nodeValue;
                        o = s.replace(t, n);
                        if (o !== s) {
                            if (!r && /</.test(o)) {
                                e(i).before(o);
                                u.push(i)
                            } else {
                                i.nodeValue = o
                            }
                        }
                    }
                } while (i = i.nextSibling)
            }
            u.length && e(u).remove()
        })
    }
})(jQuery);

$(window).scroll(function() {
    if ($(this).scrollTop()) {
        $('#back-to-top').fadeIn();
    } else {
        $('#back-to-top').fadeOut();
    }
});

$("#back-to-top").click(function() {
    $("html, body").animate({scrollTop: 0}, 1000);
 });